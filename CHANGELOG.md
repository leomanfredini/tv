# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.2] - 2017-12-06
### Added
- SÉRIE - The Big Bang Theory - T11|EP08
- SÉRIE - The Big Bang Theory - T11|EP09
- SÉRIE - Mr Robot - T3|EP06

## [0.1.1] - 2017-11-19
### Added
- SÉRIE - The Big Bang Theory - T11|EP07
- SÉRIE - Mr Robot - T3|EP04
- SÉRIE - Mr Robot - T3|EP05

## [0.1.0] - 2017-11-15
### Added
- Criação do changelog.



## [0.0.0] - 2017-11-01
### Added
- Para novos recursos. 

### Changed
- Para alterações em recursos existentes.

### Removed
- Para recursos removidos nesta versão.


